#ifndef _ESP8266_SWITCH.H
#define _ESP8266_SWITCH.H

// WiFi Connection
#define SSIDD "xxx"
#define PASSWORD "xxx"

// Device Settings
#define DEVICE_NAME "xxx"
#define DEBUG 0 //1
#define USE_RELAY 1
#define USE_BUTTON 1

#define LED_PIN X //Sonoff: 13 //NodeMCU: LED_BUILTIN
#define RELAY_PIN 12 //Sonoff: 12
#define BUTTON_PIN 0 //Sonoff: 0

// MQTT Settings
#define MQTT_SERVER "xxx"
#define MQTT_PORT 1883
#define IN_TOPIC "xxx"
#define OUT_TOPIC "xxx"

// Persist state Settings
#define PERSIST_STATE 1
#define EVENT_TOPIC "xxx"

#endif //_ESP8266_SWITCH.H
