#include "ESP8266_Switch.h"
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

int relayState = 0;
volatile unsigned long debounceButton = 0;

unsigned long delayStart = 0; // the time the delay started
unsigned long totalDelay;
bool delayRunning = false; // true if still waiting for delay to finish

WiFiClient espClient;
PubSubClient client(espClient);

// Interrupt Service Routine (ISR)
ICACHE_RAM_ATTR void buttonPressed() {

  if((millis()-debounceButton) <= 50) {
    return;
  }
  debounceButton = millis();

  // Copy contents of toggleOn and toggleOff into ISR. 
  // Do not do a function call within a ISR.
  if(relayState == 1) {
    // Turn off LED/Relay
    digitalWrite(LED_PIN, HIGH); // Turn LED Off
    relayState = 0;
    
    #if USE_RELAY
      digitalWrite(RELAY_PIN, LOW); // Relay Off
    #endif
    
    relayState = 0;
  
    // Reset timer
    delayRunning = false;
    delayStart = 0;
    totalDelay = 0;

    #if PERSIST_STATE
      if (client.connected()) {
        
        StaticJsonDocument<128> eventOut;

        eventOut["name"] = DEVICE_NAME;
        eventOut["event"] = "PRESS_OFF";

        char buffer[128];
        size_t n = serializeJson(eventOut, buffer);
        client.publish(EVENT_TOPIC, buffer);
      }
    #endif

    #if DEBUG
      Serial.println("RELAY OFF");
    #endif
    
  } else {
    // Turn on LED/Relay
    digitalWrite(LED_PIN, LOW); // LED On
    relayState = 1;
    #if USE_RELAY
      digitalWrite(RELAY_PIN, HIGH); // Relay On
    #endif
  
    relayState = 1;

    #if PERSIST_STATE
      if (client.connected()) {

        StaticJsonDocument<128> eventOut;

        eventOut["name"] = DEVICE_NAME;
        eventOut["event"] = "PRESS_ON";

        char buffer[128];
        size_t n = serializeJson(eventOut, buffer);
        client.publish(EVENT_TOPIC, buffer);
      }
    #endif
  
    #if DEBUG
      Serial.println("RELAY ON");
    #endif
  }
  // Reset pin back to low
  digitalWrite (BUTTON_PIN, HIGH);
  return;
}

void setup() {

  Serial.begin(115200);

  delay(1000);

  // Set up button interupt
  #if USE_BUTTON
    pinMode(BUTTON_PIN, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), buttonPressed, RISING);
  #endif

  pinMode(LED_PIN, OUTPUT);

  #if USE_RELAY
    pinMode(RELAY_PIN, OUTPUT);
  #endif

  //WiFi.mode(WIFI_STA);
  WiFi.begin(SSIDD, PASSWORD);
  
  while (WiFi.status() != WL_CONNECTED) {

    digitalWrite(LED_PIN, HIGH); // LED On
    delay(2000);
    digitalWrite(LED_PIN, LOW); // LED Off
    
  #if DEBUG
    Serial.println("Connecting to WiFi..");
  #endif
  }

  #if DEBUG
    Serial.println("Connected to the WiFi network");
  #endif
  
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(callback);

  digitalWrite(LED_PIN, HIGH); // LED Off

  #if USE_RELAY
    digitalWrite(RELAY_PIN, LOW); // Relay Off
  #endif

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname(DEVICE_NAME);

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    #if DEBUG
      Serial.println("Start updating " + type);
    #endif
  });
  ArduinoOTA.onEnd([]() {
    #if DEBUG
      Serial.println("\nEnd");
    #endif
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    #if DEBUG
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    #endif
  });
  ArduinoOTA.onError([](ota_error_t error) {
    #if DEBUG
    Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    #endif
  });
  ArduinoOTA.begin();
  
  #if DEBUG
    Serial.println("Ready");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  #endif
}

// ===============================================================
// Main program loop
// ===============================================================
void loop() {
  
  ArduinoOTA.handle();
  
  // MQTT connection
  if (!client.connected()) {
    reconnect();
  }

  // Check if there is a active running timer
  if (delayRunning && ((millis() - delayStart) >= totalDelay)) {
    
    #if DEBUG
      Serial.println("End of delay");
    #endif

    toggleOff();
    // Send event to update persisted state on timer timeout
    #if PERSIST_STATE
      if (client.connected()) {

        // Send timer end event
        StaticJsonDocument<128> eventOut;

        eventOut["name"] = DEVICE_NAME;
        eventOut["event"] = "TIMER_END";

        char buffer[128];
        size_t n = serializeJson(eventOut, buffer);
        client.publish(EVENT_TOPIC, buffer);
      }
    #endif
  }

  delay(100);

  client.loop();
  
}

// ===============================================================
// Handle connection to MQTT
// ===============================================================
void reconnect() {
  
  while (!client.connected()) {

    //Serial.print("Attempting MQTT connection...");

    // If you do not want to use a username and password, change next line to
    if (client.connect(DEVICE_NAME)) {
      //if (client.connect(DEVICE_NAME, mqtt_user, mqtt_password)) {

#if DEBUG
      Serial.println("Connected to MQTT");
#endif
      // Once connected, publish an announcement...
      //client.publish(IN_TOPIC, "Sonoff: booted");
      // ... and resubscribe
      client.publish(OUT_TOPIC, "Sonoff Client Connected");
      client.subscribe(IN_TOPIC);

    } else {
#if DEBUG
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
#endif
      // Wait 5 seconds before retrying
      digitalWrite(LED_PIN, HIGH); // LED On
      delay(5000);
      digitalWrite(LED_PIN, LOW); // LED Off
    }
  }
}

// ===============================================================
// Code to be run when a MQTT message is recieved
// ===============================================================
void callback(char* topic, byte* payload, unsigned int length) {

  //client.publish(IN_TOPIC, "In callback");

  StaticJsonDocument<255> doc;

  char messageBuf[256];
  int toggleState;
  int duration;
  int i = 0;
  const size_t CAPACITY = JSON_ARRAY_SIZE(10);
  const size_t CAPACITY_OBJ = JSON_OBJECT_SIZE(2);

  // Limit length of input message
  if (length > 255) {
    length = 255;
  }

  // Convert byte array to char array for processing
  for (i = 0; i < length; i++) {
    messageBuf[i] = (char)payload[i];
  }
  messageBuf[i] = '\0';

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, messageBuf);

  // Test if parsing succeeds.
  if (error) {
    //TODO: Send back error
    #if DEBUG
      Serial.print(("deserializeJson() failed: "));
      Serial.println(error.c_str());
    #endif
    
    return;
  }

  const char* msgId = doc["msgId"];
  if(msgId) {

    // Allocate the memory for the output document
    StaticJsonDocument<512> outputDoc;
    
    Serial.println(msgId);
    outputDoc["resId"] = msgId;

    JsonArray jsonArray = doc["status"].as<JsonArray>();
    if(jsonArray) {
  
      // allocate the memory for the document
      StaticJsonDocument<CAPACITY> statusDoc;
      
      // create an empty array
      JsonArray statusArray = statusDoc.to<JsonArray>();
      
      // Create json array element?
      for(JsonVariant v : jsonArray) {
        
        const char* reqStatus = v.as<char*>();
        Serial.println(reqStatus);
        
        if(strcmp(reqStatus, "ping") == 0) {
          
          StaticJsonDocument<CAPACITY_OBJ> tempDoc;
          JsonObject object = tempDoc.to<JsonObject>();
          object["ping"] = "pong";
          statusArray.add(object);

          #if DEBUG
            Serial.println("Ping... PONG!");
          #endif
          
        } else if(strcmp(reqStatus, "relay") == 0) {
          
          StaticJsonDocument<CAPACITY_OBJ> tempDoc;
          JsonObject object = tempDoc.to<JsonObject>();
          object["relay"] = relayState;
          statusArray.add(object);
          
          #if DEBUG
            Serial.print("Relay State: ");
            Serial.println(relayState);
          #endif
          
        } else if(strcmp(reqStatus, "timer") == 0) {
          
          StaticJsonDocument<CAPACITY_OBJ> tempDoc;
          JsonObject object = tempDoc.to<JsonObject>();
          
          if (delayRunning && ((millis() - delayStart) <= totalDelay)) {
            // Delay is running
            int remainingDelay = ((totalDelay - (millis() - delayStart))/1000);
            object["timer"] = remainingDelay; // Timer duration remaining (in seconds)
            
            #if DEBUG
              Serial.println("TIME LEFT: ");
              Serial.print(remainingDelay);
            #endif
            
          } else {
            // No delay running
            object["timer"] = 0;
          }
          statusArray.add(object);
          
        } else {
          
          #if DEBUG
          Serial.println("Not valid state.");
          #endif
          
        }
      }
      outputDoc["status"] = statusArray;
    }
  
    // Toggle
    toggleState = (int)doc["toggle"]["state"];
    if(toggleState) {
      if(toggleState < 0) {
        // Toggle off
        toggleOff();
        outputDoc["toggleSuccess"] = 1;
      }
      if(toggleState == 1) {
        duration = doc["toggle"]["duration"];
        // Toggle on
        if(duration) {
          #if DEBUG
          Serial.print("Duration present: ");
          Serial.println(duration);
          #endif
          delayRunning = true;
          delayStart = millis();
          totalDelay = duration*1000; // Duration is in seconds, convert to milliseconds
        }
        toggleOn();
        outputDoc["toggleSuccess"] = 1;
      }
    }

    char buffer[512];
    size_t n = serializeJson(outputDoc, buffer);
    client.publish(OUT_TOPIC, buffer);

    #if DEBUG
    Serial.println(buffer);
    #endif
  
    return;
  }
  // No message id, do nothing
  return;
}

// ===============================================================
// Function to Toggle Relay Off
// ===============================================================
void toggleOff() {
  
  digitalWrite(LED_PIN, HIGH); // Turn LED Off
  #if USE_RELAY
    digitalWrite(RELAY_PIN, LOW); // Relay Off
  #endif
    
  relayState = 0;
  
  // Reset timer
  delayRunning = false;
  delayStart = 0;
  totalDelay = 0;

  #if DEBUG
    Serial.println("RELAY OFF");
  #endif
}

// ===============================================================
// Function to Toggle Relay On
// ===============================================================
void toggleOn() {

  digitalWrite(LED_PIN, LOW); // LED On
  #if USE_RELAY
    digitalWrite(RELAY_PIN, HIGH); // Relay On
  #endif

  relayState = 1;

  #if DEBUG
    Serial.println("RELAY ON");
  #endif
}
