#ifndef _H801LEDCONTROLLER.H
#define _H801LEDCONTROLLER.H

#define DEVICE "H801"

// WiFi Connection
#define SSIDD "xxx"
#define PASSWORD "xxx"

// Device Settings
#define DEVICE_NAME "xxx"
#define DEBUG 0
#define ACTIVE_PIN_MODE 0 // Used for testing on a generic esp8266

// MQTT Settings
#define MQTT_SERVER "xxx"
#define MQTT_PORT 1883
#define IN_TOPIC "xxx"
#define OUT_TOPIC "xxx"

#define LED_GREEN_PIN    1
#define LED_RED_PIN      5

#define RGB_LIGHT_RED_PIN    15
#define RGB_LIGHT_GREEN_PIN  13
#define RGB_LIGHT_BLUE_PIN   12
#define W1_PIN               14
#define W2_PIN               4

#endif //_H801LEDCONTROLLER.H
