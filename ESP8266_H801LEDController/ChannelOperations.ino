// ===============================================================
// TOGGLE ON
// ===============================================================
void parseToggleOn(JsonObject outputDoc, JsonObject toggleInput) {

  JsonObject rgbObject = toggleInput["rgb"].as<JsonObject>();
  if (rgbObject) {

    rgb.r = rgbObject["r"];
    rgb.g = rgbObject["g"];
    rgb.b = rgbObject["b"];

    int brightness = rgbObject["br"];
    rgb.brightness = scaleBrightness(brightness);

    int timerSeconds = rgbObject["timer"];
    if (timerSeconds > 0) {
      rgb.timer = (timerSeconds * 1000); //Convert seconds to milliseconds
    } else {
      rgb.timer = 0;
    }

    turnOnRGB();

    rgb.active = 1;
    
    outputDoc["toggle_on"]["rgb"] = 1;
  }

  JsonObject tunableObject = toggleInput["tun"].as<JsonObject>();
  if (tunableObject) {

    int brightness = tunableObject["br"];
    if (brightness < 0) brightness = 0;
    if (brightness > 100) brightness = 100;

    int wwVal = tunableObject["ww"];
    int cwVal = tunableObject["cw"];

    float scale = brightness * .01;
    ww.brightness = wwVal * scale;
    cw.brightness = cwVal * scale;
    tunable.brightness = brightness;

    // Keep from crashing board
    if(ww.brightness > 190 && cw.brightness > 190) {
      outputDoc["toggle_on"]["tun"] = 0;
      outputDoc["toggle_on"]["err"] = "WW/CW channel both exceed 75% bright";
      return;
    }

    int timerSeconds = tunableObject["timer"];
    if (timerSeconds > 0) {
      tunable.timer = (timerSeconds * 1000); //Convert seconds to milliseconds
    } else {
      tunable.timer = 0;
    }

    setWWStateOff();
    setCWStateOff();

    turnOnTunable();

    tunable.active = 1;
    
    outputDoc["toggle_on"]["tun"] = 1;
  }

  JsonObject wwObject = toggleInput["ww"].as<JsonObject>();
  if (wwObject) {

    int brightness = wwObject["br"];
    ww.brightness = scaleBrightness(brightness);

    // Cancel tunable active
    if(tunable.active) {
      turnOffTunable();
    }

    // Keep from crashing board, turn off other white channel
    if(ww.brightness > 190 && cw.brightness > 190) {
      turnOffCW();
      setCWStateOff();
    }

    int timerSeconds = wwObject["timer"];
    if (timerSeconds > 0) {
      ww.timer = (timerSeconds * 1000); //Convert seconds to milliseconds
    } else {
      ww.timer = 0;
    }

    setTunableStateOff();

    turnOnWW();
    
    ww.active = 1;
    
    outputDoc["toggle_on"]["ww"] = 1;
  }

  JsonObject cwObject = toggleInput["cw"].as<JsonObject>();
  if (cwObject) {

    int brightness = cwObject["br"];
    cw.brightness = scaleBrightness(brightness);

    // Cancel tunable active
    if(tunable.active) {
      turnOffTunable();
    }

    // Keep from crashing board, turn off other white channel
    if(ww.brightness > 190 && cw.brightness > 190) {
      turnOffWW();
      setWWStateOff();
    }

    int timerSeconds = cwObject["timer"];
    if (timerSeconds > 0) {
      cw.timer = (timerSeconds * 1000); //Convert seconds to milliseconds
    } else {
      cw.timer = 0;
    }

    setTunableStateOff();

    turnOnCW();
    
    cw.active = 1;
    
    outputDoc["toggle_on"]["cw"] = 1;
  }

}

// ===============================================================
// TOGGLE OFF
// ===============================================================
void parseToggleOff(JsonObject outputDoc, JsonArray toggleInput) {

  for (JsonVariant v : toggleInput) {

    const char* reqStatus = v.as<char*>();

    if (strcmp(reqStatus, "rgb") == 0) {
      
      turnOffRGB();

      setRGBStateOff();
  
      outputDoc["toggle_off"]["rgb"] = 1;

    } else if (strcmp(reqStatus, "tun") == 0) {
      
      turnOffTunable();

      setTunableStateOff();
  
      outputDoc["toggle_off"]["tun"] = 1;

    } else if (strcmp(reqStatus, "ww") == 0) {
      
      turnOffWW();

      setWWStateOff();
      
      outputDoc["toggle_off"]["ww"] = 1;

    } else if (strcmp(reqStatus, "cw") == 0) {
      
      turnOffCW();
      
      setCWStateOff();
      
      outputDoc["toggle_off"]["cw"] = 1;

    } else {

#if DEBUG
      Serial.println("Toggle Off: Invalid Channel Recieved");
#endif
      outputDoc["toggle_off"] = 0;
      outputDoc["toggle_off"]["err"] = "Invalid Channel";

    }
  }
}

void setRGBStateOff() {
  rgb.start = 0;
  rgb.timer_active = 0;
  rgb.timer = 0;
  rgb.active = 0;
}

void setTunableStateOff() {
  tunable.start = 0;
  tunable.timer_active = 0;
  tunable.timer = 0;
  tunable.active = 0;
}

void setWWStateOff() {
  ww.start = 0;
  ww.timer_active = 0;
  ww.timer = 0;
  ww.active = 0;
}

void setCWStateOff() {
  cw.start = 0;
  cw.timer_active = 0;
  cw.timer = 0;
  cw.active = 0;
}

// ===============================================================
// RGB
// ===============================================================
void turnOnRGB() {

#if ACTIVE_PIN_MODE
  //analogWrite(RGB_LIGHT_RED_PIN, map(p_red, 0, 255, 0, m_rgb_brightness));
  //analogWrite(RGB_LIGHT_GREEN_PIN, map(p_green, 0, 255, 0, m_rgb_brightness));
  //analogWrite(RGB_LIGHT_BLUE_PIN, map(p_blue, 0, 255, 0, m_rgb_brightness));
  analogWrite(RGB_LIGHT_RED_PIN, map(rgb.r, 0, 255, 0, rgb.brightness));
  analogWrite(RGB_LIGHT_GREEN_PIN, map(rgb.g, 0, 255, 0, rgb.brightness));
  analogWrite(RGB_LIGHT_BLUE_PIN, map(rgb.b, 0, 255, 0, rgb.brightness));
#endif

  if (rgb.timer > 0) {

    rgb.start = millis();
    rgb.timer_active = 1;

#if DEBUG
    Serial.println("RGB Timer Set");
#endif
  }

#if DEBUG
  Serial.println("RGB Channels On");

  Serial.print("Red:");
  Serial.println(rgb.r);
  Serial.print("Green:");
  Serial.println(rgb.g);
  Serial.print("Blue:");
  Serial.println(rgb.b);
  Serial.print("Brightness:");
  Serial.println(rgb.brightness);
  Serial.print("Timer:");
  Serial.println(rgb.timer);
#endif
}

void turnOffRGB() {

#if ACTIVE_PIN_MODE
  analogWrite(RGB_LIGHT_RED_PIN, map(0, 0, 255, 0, 0));
  analogWrite(RGB_LIGHT_GREEN_PIN, map(0, 0, 255, 0, 0));
  analogWrite(RGB_LIGHT_BLUE_PIN, map(0, 0, 255, 0, 0));
#endif

#if DEBUG
  Serial.println("RGB Channels Off");
#endif
}


// ===============================================================
// TUNABLE
// ===============================================================
void turnOnTunable() {

  turnOnWW();
  turnOnCW();

  if (tunable.timer > 0) {

    tunable.start = millis();
    tunable.timer_active = 1;

#if DEBUG
    Serial.println("Tunable Timer Set");
#endif
  }

#if DEBUG
  Serial.println("Tunable Channels On");

  Serial.print("Tunable WW:");
  Serial.println(ww.brightness);
  Serial.print("Tunable CW:");
  Serial.println(cw.brightness);
#endif
}

void turnOffTunable() {

  turnOffWW();
  turnOffCW();

#if DEBUG
  Serial.println("Tunable Channels Off");
#endif
}

// ===============================================================
// WW
// ===============================================================
void turnOnWW() {

#if ACTIVE_PIN_MODE
  // convert the brightness in % (0-100%) into a digital value (0-255)
  //analogWrite(W1_PIN, brightness);
  analogWrite(W1_PIN, ww.brightness);
#endif

  if (ww.timer > 0) {

    ww.start = millis();
    ww.timer_active = 1;

#if DEBUG
    Serial.println("WW Timer Set");
#endif
  }

#if DEBUG
  Serial.println("WW Channel On");

  Serial.print("Tunable WW:");
  Serial.println(ww.brightness);
#endif

}

void turnOffWW() {

#if ACTIVE_PIN_MODE
  analogWrite(W1_PIN, 0);
#endif

}

// ===============================================================
// CW
// ===============================================================
void turnOnCW() {

#if ACTIVE_PIN_MODE
  analogWrite(W2_PIN, cw.brightness);
#endif

  if (cw.timer > 0) {

    cw.start = millis();
    cw.timer_active = 1;

#if DEBUG
    Serial.println("CW Timer Set");
#endif
  }

#if DEBUG
  Serial.println("CW Channel On");

  Serial.print("Tunable CW:");
  Serial.println(cw.brightness);
#endif
}


void turnOffCW() {

#if ACTIVE_PIN_MODE
  analogWrite(W2_PIN, 0);
#endif
}

int scaleBrightness(int inputBrightness) {

  if (inputBrightness < 0) inputBrightness = 0;
  if (inputBrightness > 100) inputBrightness = 100;

  float scale = inputBrightness * .01;
  int scaledBrightness = scale * 255;
  return scaledBrightness;
}
