#include "ESP8266_H801LEDController.h"
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

#include "ChannelOperations.h"

struct Rgb {
  bool active = 0; // 1 or 0
  uint8_t r = 0; // 0 - 255
  uint8_t g = 0; // 0 - 255
  uint8_t b = 0; // 0 - 255
  uint8_t brightness = 0; // 0 - 255 (input 0 - 100)
  bool timer_active = 0; // true of false
  unsigned long timer = 0; // length of timer in msecs
  unsigned long start = 0;
} rgb;

struct Tunable {
  bool active = 0; // 1 or 0
  uint8_t brightness = 0; // 0 - 255 (input 0 - 100)
  bool timer_active = 0; // true of false
  unsigned long timer = 0; // length of timer in msecs
  unsigned long start = 0;
} tunable;

struct WW {
  bool active = 0; // 1 or 0
  uint8_t brightness = 0; // 0 - 255 (input 0 - 100)
  bool timer_active = 0; // true of false
  unsigned long timer = 0; // length of timer in msecs
  unsigned long start = 0;
} ww;

struct CW {
  bool active = 0; // 1 or 0
  uint8_t brightness = 0; // 0 - 255 (input 0 - 100)
  bool timer_active = 0; // true of false
  unsigned long timer = 0; // length of timer in msecs
  unsigned long start = 0;
} cw;

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {

  Serial.begin(115200);

  delay(1000);

  #if ACTIVE_PIN_MODE
    pinMode(RGB_LIGHT_RED_PIN, OUTPUT);
    pinMode(RGB_LIGHT_GREEN_PIN, OUTPUT);
    pinMode(RGB_LIGHT_BLUE_PIN, OUTPUT);
    analogWriteRange(255);
    turnOffRGB();
    delay(500);
    pinMode(W1_PIN, OUTPUT);
    turnOffWW();
    delay(500);
    pinMode(W2_PIN, OUTPUT);
    turnOffCW();

    pinMode(LED_GREEN_PIN, OUTPUT);
    pinMode(LED_RED_PIN, OUTPUT);
    digitalWrite(LED_RED_PIN, LOW);
    digitalWrite(LED_GREEN_PIN, LOW);
  #endif

  WiFi.begin(SSIDD, PASSWORD);
  
  while (WiFi.status() != WL_CONNECTED) {

    delay(1000);

    #if ACTIVE_PIN_MODE
      digitalWrite(LED_GREEN_PIN, HIGH); // LED On
      delay(2000);
      digitalWrite(LED_GREEN_PIN, LOW); // LED Off
    #endif
    
    #if DEBUG
      Serial.println("Connecting to WiFi..");
    #endif
  }

  #if DEBUG
    Serial.println("Connected to the WiFi network");
  #endif
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(callback);

  #if ACTIVE_PIN_MODE
    digitalWrite(LED_GREEN_PIN, HIGH); // LED Off
  #endif

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);


  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname(DEVICE_NAME);

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    #if DEBUG
      Serial.println("Start updating " + type);
    #endif
  });
  ArduinoOTA.onEnd([]() {
    #if DEBUG
      Serial.println("\nEnd");
    #endif
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    #if DEBUG
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    #endif
  });
  ArduinoOTA.onError([](ota_error_t error) {
    #if DEBUG
    Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    #endif
  });
  ArduinoOTA.begin();
  
  #if DEBUG
    Serial.println("Ready");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  #endif
}

void loop() {

  ArduinoOTA.handle();
  
  // MQTT connection
  if (!client.connected()) {
    reconnect();
  }
  
  checkActiveTimers();

  delay(1000);

  client.loop();
}

void checkActiveTimers() {

  if(rgb.timer_active) {
    if((millis() - rgb.start) >= rgb.timer) {
      #if DEBUG
        Serial.println("RGB Timer Expired");
      #endif
      turnOffRGB();
      setRGBStateOff();

      #if PERSIST_STATE
        if (client.connected()) {
  
          // Send timer end event
          StaticJsonDocument<128> eventOut;
  
          eventOut["name"] = DEVICE_NAME;
          eventOut["event"] = "TIMER_END_RGB";
  
          char buffer[128];
          size_t n = serializeJson(eventOut, buffer);
          client.publish(EVENT_TOPIC, buffer);
        }
      #endif
    }
  }
  if(tunable.timer_active) {
    if((millis() - tunable.start) >= tunable.timer) {
      #if DEBUG
        Serial.println("Tunable Timer Expired");
      #endif
      turnOffTunable();
      setTunableStateOff();

      #if PERSIST_STATE
        if (client.connected()) {
  
          // Send timer end event
          StaticJsonDocument<128> eventOut;
  
          eventOut["name"] = DEVICE_NAME;
          eventOut["event"] = "TIMER_END_TUNABLE";
  
          char buffer[128];
          size_t n = serializeJson(eventOut, buffer);
          client.publish(EVENT_TOPIC, buffer);
        }
      #endif
    }
  }
  if(ww.timer_active) {
    if((millis() - ww.start) >= ww.timer) {
      #if DEBUG
        Serial.println("WW Timer Expired");
      #endif
      turnOffWW();
      setWWStateOff();

      #if PERSIST_STATE
        if (client.connected()) {
  
          // Send timer end event
          StaticJsonDocument<128> eventOut;
  
          eventOut["name"] = DEVICE_NAME;
          eventOut["event"] = "TIMER_END_WW";
  
          char buffer[128];
          size_t n = serializeJson(eventOut, buffer);
          client.publish(EVENT_TOPIC, buffer);
        }
      #endif
    }
  }
  if(cw.timer_active) {
    if((millis() - cw.start) >= cw.timer) {
      #if DEBUG
        Serial.println("CW Timer Expired");
      #endif
      turnOffCW();
      setCWStateOff();

      #if PERSIST_STATE
        if (client.connected()) {
  
          // Send timer end event
          StaticJsonDocument<128> eventOut;
  
          eventOut["name"] = DEVICE_NAME;
          eventOut["event"] = "TIMER_END_CW";
  
          char buffer[128];
          size_t n = serializeJson(eventOut, buffer);
          client.publish(EVENT_TOPIC, buffer);
        }
      #endif
    }
  }
  
}

// ===============================================================
// Code to be run when a MQTT message is recieved
// ===============================================================
void callback(char* topic, byte* payload, unsigned int length) {

  //client.publish(IN_TOPIC, "In callback");
  DynamicJsonDocument doc(512);
  //StaticJsonDocument<511> doc;

  char messageBuf[512];
  int toggleState;
  int duration;
  int i = 0;
  const size_t CAPACITY = JSON_ARRAY_SIZE(10);
  
  // Limit length of input message
  if (length > 511) {
    length = 511;
  }

  // Convert byte array to char array for processing
  for (i = 0; i < length; i++) {
    messageBuf[i] = (char)payload[i];
  }
  messageBuf[i] = '\0';

  Serial.println(messageBuf);

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, messageBuf);
  // Test if parsing succeeds.
  if (error) {
    //TODO: Send back error
    #if DEBUG
      Serial.print(("deserializeJson() failed: "));
      Serial.println(error.c_str());
    #endif
    
    return;
  }

  const char* msgId = doc["msgId"];
  if(msgId) {

    // Allocate the memory for the output document
    const size_t capacity = 1024;
    DynamicJsonDocument outputDoc(capacity);
    //StaticJsonDocument<1024> outputDoc;
    JsonObject outputDocObject = outputDoc.to<JsonObject>();
    
    outputDocObject["resId"] = msgId;
    
    JsonArray jsonArray = doc["status"].as<JsonArray>();
    if(jsonArray) {
      
      JsonArray jsonArray = doc["status"].as<JsonArray>();
      
      // Create JSON object
      StaticJsonDocument<1024> statusReturnDoc;
      JsonObject statusReturnObj = statusReturnDoc.to<JsonObject>();

      generateStatus(statusReturnObj, jsonArray);
      outputDoc["status"] = statusReturnObj;

      // allocate the memory for the document
      //StaticJsonDocument<100> statusDoc;
      
      // create an empty array
      //JsonObject statusArray = statusDoc.to<JsonObject>();
      
      //outputDoc["status"] = statusArray;
      
    }

    JsonObject toggleOnObject = doc["toggle_on"].as<JsonObject>();
    if(toggleOnObject) {
      parseToggleOn(outputDocObject, toggleOnObject);
      
    }

    JsonArray toggleOffArray = doc["toggle_off"].as<JsonArray>();
    if(toggleOffArray) {

      parseToggleOff(outputDocObject, toggleOffArray);
    }

    char buffer[2048];
    size_t n = serializeJson(outputDocObject, buffer);
    
    client.publish(OUT_TOPIC, buffer);

    #if DEBUG
      //Serial.println(buffer);
    #endif
    }
    
  return;
}


void generateStatus(JsonObject statusReturnObj, JsonArray requestedStatusArray) {

  for(JsonVariant v : requestedStatusArray) {
    
    const char* reqStatus = v.as<char*>();

    if(strcmp(reqStatus, "ping") == 0) {
      
      statusReturnObj["ping"] = "pong";

      #if DEBUG
        Serial.println("Ping... PONG!");
      #endif
      
    } else if(strcmp(reqStatus, "state") == 0) {

      statusReturnObj["state"]["rgb"] = rgb.active;
      statusReturnObj["state"]["tun"] = tunable.active;
      statusReturnObj["state"]["ww"] = ww.active;
      statusReturnObj["state"]["cw"] = cw.active;
      
      #if DEBUG
        Serial.println("Get Channel States");
      #endif
      
    } else if(strcmp(reqStatus, "timers") == 0) {

      statusReturnObj["timers"]["rgb"]["active"] = rgb.timer_active;
      if(rgb.timer_active) {
        int remainingDelay = ((rgb.timer - (millis() - rgb.start))/1000);
        statusReturnObj["timers"]["rgb"]["timer"] = remainingDelay;
      }
      statusReturnObj["timers"]["tun"]["active"] = tunable.timer_active;
      if(tunable.timer_active) {
        int remainingDelay = ((tunable.timer - (millis() - tunable.start))/1000);
        statusReturnObj["timers"]["tun"]["timer"] = remainingDelay;
      }
      statusReturnObj["timers"]["ww"]["active"] = ww.timer_active;
      if(ww.timer_active) {
        int remainingDelay = ((ww.timer - (millis() - ww.start))/1000);
        statusReturnObj["timers"]["ww"]["timer"] = remainingDelay;
      }
      statusReturnObj["timers"]["cw"]["active"] = cw.timer_active;
      if(cw.timer_active) {
        int remainingDelay = ((cw.timer - (millis() - cw.start))/1000);
        statusReturnObj["timers"]["cw"]["timer"] = remainingDelay;
      }
      
      #if DEBUG
        Serial.println("Get Channel Timers");
      #endif
      
    } else {

      statusReturnObj[reqStatus]["err"] = "Invalid State";
      
      #if DEBUG
        Serial.println("Invalid State Requested");
      #endif
      
    }
     
  }
}

// ===============================================================
// Handle connection to MQTT
// ===============================================================
void reconnect() {

  while (!client.connected()) {

    Serial.print("Attempting MQTT connection...");

    // If you do not want to use a username and password, change next line to
    if (client.connect(DEVICE_NAME)) {
      //if (client.connect(DEVICE_NAME, mqtt_user, mqtt_password)) {

      #if DEBUG
        Serial.println("Connected to MQTT");
      #endif
      // Once connected, publish an announcement...
      //client.publish(IN_TOPIC, "Sonoff: booted");
      // ... and resubscribe
      client.publish(OUT_TOPIC, "Sonoff Client Connected");
      client.subscribe(IN_TOPIC);

    } else {
#if DEBUG
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
#endif
      // Wait 5 seconds before retrying
      #if ACTIVE_PIN_MODE
        digitalWrite(LED_GREEN_PIN, HIGH); // LED On
      #endif
        delay(5000);
      #if ACTIVE_PIN_MODE
        digitalWrite(LED_GREEN_PIN, LOW); // LED Off
      #endif
    }
  }
}
