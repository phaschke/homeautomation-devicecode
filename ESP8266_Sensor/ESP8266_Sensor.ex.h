#ifndef _ESP8266_SENSOR.H
#define _ESP8266_SENSOR.H

// WiFi Connection
#define SSIDD "xxx"
#define PASSWORD "xxx"

// Device Settings
#define DEVICE_NAME "xxx"
#define DEBUG 0

// LED
#define LED_PIN LED_BUILTIN

// MQTT Settings
#define MQTT_SERVER "xxx"
#define MQTT_PORT 1883
#define IN_TOPIC "xxx"
#define OUT_TOPIC "xxx"

// Event Settings
#define USE_EVENTS 1
#define EVENT_TOPIC "event/in"

//Specific Sensor Settings
#define ONEWIREBUS 12 // GPIO where the DS18B20 is connected to

#endif //_ESP8266_SENSOR.H
