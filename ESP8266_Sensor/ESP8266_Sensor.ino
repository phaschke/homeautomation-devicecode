#include "ESP8266_Sensor.h"
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

//-- Dallas One wire --
#include <OneWire.h>
#include <DallasTemperature.h>
//--
//-- BME280 --
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
//--

// Global status variables
boolean useBME = true;

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONEWIREBUS);
// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);

//#define SEALEVELPRESSURE_HPA (1013.25)
Adafruit_BME280 bme; // I2C

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {

  Serial.begin(115200);

  delay(3000);

  WiFi.begin(SSIDD, PASSWORD);

  pinMode(LED_PIN, OUTPUT);

  while (WiFi.status() != WL_CONNECTED) {

    digitalWrite(LED_PIN, HIGH); // LED On
    delay(2000);

    digitalWrite(LED_PIN, LOW); // LED Off
    
    #if DEBUG
      Serial.println("Connecting to WiFi..");
    #endif
  }

  #if DEBUG
    Serial.println("Connected to the WiFi network");
  #endif
  
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(callback);

  digitalWrite(LED_PIN, HIGH);

  // One wire Initialization (For DS18B20)
  sensors.begin();

  // BME280 Initialization
  bool statusBME;
  statusBME = bme.begin(0x76);  
  if (!statusBME) {
    #if DEBUG
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    #endif
    // Do not block on BME connection
    useBME = false;
  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname(DEVICE_NAME);

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    #if DEBUG
      Serial.println("Start updating " + type);
    #endif
  });
  ArduinoOTA.onEnd([]() {
    #if DEBUG
      Serial.println("\nEnd");
    #endif
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    #if DEBUG
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    #endif
  });
  ArduinoOTA.onError([](ota_error_t error) {
    #if DEBUG
    Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    #endif
  });
  ArduinoOTA.begin();
  
  #if DEBUG
    Serial.println("Ready");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  #endif

}

// ===============================================================
// Main program loop
// ===============================================================
void loop() {

  ArduinoOTA.handle();

  // MQTT connection
  if (!client.connected()) {
    reconnect();
  }

  delay(100);

  client.loop();
}



// ===============================================================
// Handle connection to MQTT
// ===============================================================
void reconnect() {

  while (!client.connected()) {

    //Serial.print("Attempting MQTT connection...");

    // If you do not want to use a username and password, change next line to
    if (client.connect(DEVICE_NAME)) {
      //if (client.connect(DEVICE_NAME, mqtt_user, mqtt_password)) {

#if DEBUG
      Serial.println("Connected to MQTT");
#endif
      // Once connected, publish an announcement...
      //client.publish(IN_TOPIC, "Sonoff: booted");
      // ... and resubscribe
      client.publish(OUT_TOPIC, "Sonoff Client Connected");
      client.subscribe(IN_TOPIC);

    } else {
#if DEBUG
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
#endif
      // Wait 5 seconds before retrying
      digitalWrite(LED_PIN, HIGH); // LED On
      delay(5000);
      digitalWrite(LED_PIN, LOW); // LED Off
    }
  }
}
// ===============================================================
// Code to be run when a MQTT message is recieved
// ===============================================================
void callback(char* topic, byte* payload, unsigned int length) {

  //client.publish(IN_TOPIC, "In callback");

  StaticJsonDocument<255> doc;

  char messageBuf[256];
  int toggleState;
  int duration;
  int i = 0;
  const size_t CAPACITY = JSON_ARRAY_SIZE(10);
  const size_t CAPACITY_OBJ = JSON_OBJECT_SIZE(2);

  char temperature[3];
  char humidity[3];

  // Limit length of input message
  if (length > 255) {
    length = 255;
  }

  // Convert byte array to char array for processing
  for (i = 0; i < length; i++) {
    messageBuf[i] = (char)payload[i];
  }
  messageBuf[i] = '\0';

   // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, messageBuf);

  // Test if parsing succeeds.
  if (error) {
    //TODO: Send back error
    #if DEBUG
      Serial.print(("deserializeJson() failed: "));
      Serial.println(error.c_str());
    #endif
    
    return;
  }

  const char* msgId = doc["msgId"];
  if(msgId) {

    // Allocate the memory for the output document
    StaticJsonDocument<512> outputDoc;
    
    Serial.println(msgId);
    outputDoc["resId"] = msgId;

    JsonArray jsonArray = doc["status"].as<JsonArray>();
    if(jsonArray) {
  
      // allocate the memory for the document
      StaticJsonDocument<CAPACITY> statusDoc;
      
      // create an empty array
      JsonArray statusArray = statusDoc.to<JsonArray>();
      
      // Create json array element?
      for(JsonVariant v : jsonArray) {
        
        const char* reqStatus = v.as<char*>();
        //Serial.println(reqStatus);
        
        if(strcmp(reqStatus, "ping") == 0) {
          
          StaticJsonDocument<CAPACITY_OBJ> tempDoc;
          JsonObject object = tempDoc.to<JsonObject>();
          object["ping"] = "pong";
          statusArray.add(object);

          #if DEBUG
            Serial.println("Ping... PONG!");
          #endif
          
        } else if(strcmp(reqStatus, "temp") == 0) {

          StaticJsonDocument<CAPACITY_OBJ> tempDoc;
          JsonObject object = tempDoc.to<JsonObject>();
          
          #if DEBUG
            Serial.println("Get temp");
          #endif
      
          sensors.requestTemperatures();
          
          //float temperatureC = sensors.getTempCByIndex(0);
          float temperatureF = sensors.getTempFByIndex(0);
          sprintf(temperature, "%0.1f\0", temperatureF);

          #if DEBUG
            Serial.print("Current Temp: ");
            Serial.print(temperature);
            Serial.println("ºF");
          #endif
          
          object["temp"] = temperature;
          statusArray.add(object);
          
        } else if(strcmp(reqStatus, "humid") == 0) {

          #if DEBUG
            Serial.println("Get humid");
          #endif
          
          StaticJsonDocument<CAPACITY_OBJ> tempDoc;
          JsonObject object = tempDoc.to<JsonObject>();
        
          if(useBME) {
            float humidityF = bme.readHumidity();
            sprintf(humidity, "%0.1f\0", humidityF);
            object["humid"] = humidity;
            
          } else {
            object["humid"] = -1;
          }
          
          statusArray.add(object);
          
        } else {
          
          #if DEBUG
          Serial.println("Not valid state.");
          #endif
          
        }
      }
      outputDoc["status"] = statusArray;
    }

    char buffer[512];
    size_t n = serializeJson(outputDoc, buffer);
    client.publish(OUT_TOPIC, buffer);

    #if DEBUG
    Serial.println(buffer);
    #endif
  
    return;
  }
  // No message id, do nothing
  return;
}
